// Soal no. 1
console.log("Soal No. 1 (Array to Object)");
console.log('------------');
var now = new Date();
var thisYear = now.getFullYear();
var tahunInt
var tahunStr
var age
var namaLengkap = {}
function arrayToObject(arr) {
    if (arr[0] == null) {
        console.log('" ');
    } else {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][3] == null || arr[i][3] > thisYear) {
                tahunStr = "Invalid Birth Year"
            } else {
                tahunInt = thisYear - arr[i][3]
                tahunStr = tahunInt
            }
            var detail = {}
            detail.firstName = arr[i][0]
            detail.lastName = arr[i][1]
            detail.gender = arr[i][2]
            detail.age = tahunStr
            namaLengkap[arr[i][0] + arr[i][1]] = detail
            console.log(namaLengkap);
            namaLengkap = {}
            detail = {}
        }
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)


// Soal no. 2
console.log("\nSoal No. 2 (Shopping Time)");
console.log('------------');
var nama2 = {}
var arr2 = []
function shoppingTime(memberId, money) {
    var sisa
    if (memberId != memberId) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }
    nama2.memberId = memberId
    nama2.money = money
    sisa = nama2.money
    if (sisa >= 1500000) {
        arr2.push("Sepatu Stacattu")
        sisa -= 1500000
    }
    if (sisa >= 500000) {
        arr2.push("Baju Zoro")
        sisa -= 500000
    }
    if (sisa >= 250000) {
        arr2.push("Baju H&N")
        sisa -= 250000
    }
    if (sisa >= 175000) {
        arr2.push("Sweater Uniklooh")
        sisa -= 175000
    }
    if (sisa >= 50000) {
        arr2.push("Casing Handphone")
        sisa -= 50000
    }
    nama2.listPurchased = arr2
    nama2.changemoney = sisa
    arr2 = []
    sisa = 0
    return (nama2)
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));

// Soal no. 3
console.log("\nSoal No. 3 (Naik Angkot)");
console.log('------------');
var tagihanAwal
var tagihanAkhir
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if (arrPenumpang.length == 0) {
        return "[]"
    }
    for (var i = 0; i < rute.length; i++) {
        if (arrPenumpang[i][1] == rute[i]) {
            tagihanAwal = i
        }
        if (arrPenumpang[i][2] == rute[i]) {
            tagihanAkhir = i
        }
        hasil = (tagihanAkhir - tagihanAwal) * 2000
        console.log(arrPenumpang[i][2]);
        
        var detailPelanggan = {};
        detailPelanggan.penumpang = arrPenumpang[i][0]
        detailPelanggan.naikDari = arrPenumpang[i][1]
        detailPelanggan.tujuan = arrPenumpang[i][2]
        detailPelanggan.bayar = hasil
        return detailPelanggan;
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

// console.log(naikAngkot([])); //[]