import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import data from './data.json';
import VideoItem from './components/VideoItem';

export default class App extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View
                    style={{
                        height: 55,
                        backgroundColor: 'white',
                        paddingHorizontal: 15,
                        elevation: 3,
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                    }} >
                    <Image source={require('../Tugas12/images/logo.png')} style={{ height: 22, width: 98 }} />
                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity>
                            <Ionicons style={{ marginRight: 25 }} name="md-search" size={24} color="black" />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <MaterialIcons name="account-circle" size={24} color="black" />
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ flex: 1 }} >
                    <FlatList
                        data={data.items}
                        renderItem={(video) => <VideoItem video={video.item} />}
                        keyExtractor={(item) => item.id}
                        ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
                    />
                </View>

                <View
                    style={{
                        height: 60,
                        backgroundColor: 'red',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        backgroundColor: 'white',
                        borderColor: '#E5E5E5',
                    }}
                >
                    <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <MaterialIcons name="home" size={25} color="#3c3c3c" />
                        <Text style={{ fontSize: 11, color: '#3c3c3c' }}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <MaterialIcons name="whatshot" size={25} color="#3c3c3c" />
                        <Text style={{ fontSize: 11, color: '#3c3c3c' }}>Tranding</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <MaterialIcons name="subscriptions" size={25} color="#3c3c3c" />
                        <Text style={{ fontSize: 11, color: '#3c3c3c' }}>Subscriptions</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <MaterialIcons name="folder" size={25} color="#3c3c3c" />
                        <Text style={{ fontSize: 11, color: '#3c3c3c' }}>Library</Text>
                    </TouchableOpacity>
                </View>
            </View >
        );
    }
}
