import React, { Component } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import { MaterialCommunityIcons, FontAwesome, AntDesign } from '@expo/vector-icons';

export default class AboutScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                <ScrollView>
                    <View style={{ marginTop: 64, alignItems: 'center', marginHorizontal: 8 }}>
                        <Text style={{ fontSize: 36, fontWeight: 'bold', color: '#003366' }}> Tentang Saya </Text>
                        <View
                            style={{
                                backgroundColor: '#EFEFEF',
                                width: 200,
                                height: 200,
                                borderRadius: 100,
                                marginTop: 12,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                            <MaterialCommunityIcons name="account" size={190} color="#CACACA" />
                        </View>
                        <Text style={{ fontSize: 24, color: '#003366', marginTop: 24, fontWeight: 'bold' }}>Rahmat januardi</Text>
                        <Text style={{ fontSize: 16, color: '#3EC6FF', marginTop: 8, fontWeight: 'bold' }}>React Native Developer</Text>
                    </View>
                    <View
                        style={{
                            height: 140,
                            backgroundColor: '#EFEFEF',
                            borderRadius: 16,
                            marginTop: 16,
                            marginBottom: 10,
                            marginHorizontal: 8,
                            padding: 8,
                        }}
                    >
                        <Text style={{ fontSize: 18, color: '#003366', marginBottom: 8 }}>Portofolio</Text>
                        <View style={{ borderWidth: 1, borderColor: '#003366' }} />
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', flex: 1 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <FontAwesome name="gitlab" size={42} color="#3EC6FF" />
                                <Text style={{ fontSize: 16, color: '#003366' }}>@Rahmat_J</Text>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                                <AntDesign name="github" size={42} color="#3EC6FF" />
                                <Text style={{ fontSize: 16, color: '#003366' }}>@Rahmat96</Text>
                            </View>
                        </View>
                    </View>
                    <View
                        style={{
                            height: 251,
                            backgroundColor: '#EFEFEF',
                            borderRadius: 16,
                            marginBottom: 60,
                            marginHorizontal: 8,
                            padding: 8.
                        }}
                    >
                        <Text style={{ fontSize: 18, color: '#003366', marginBottom: 8 }}>Hubungi Saya</Text>
                        <View style={{ borderWidth: 1, borderColor: '#003366' }} />
                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, paddingVertical: 20 }}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                                <AntDesign name="facebook-square" size={42} color="#3EC6FF" />
                                <Text style={{ marginLeft: 20, fontSize: 16, color: '#003366' }}>rahmatjanuardi96</Text>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row', marginVertical: 30 }}>
                                <AntDesign name="instagram" size={42} color="#3EC6FF" />
                                <Text style={{ marginLeft: 20, fontSize: 16, color: '#003366' }}>@rahmatjanuardi96</Text>
                            </View>
                            <View style={{ alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
                                <AntDesign name="twitter" size={42} color="#3EC6FF" />
                                <Text style={{ marginLeft: 20, fontSize: 16, color: '#003366' }}>@rahmatjanuardi4</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
