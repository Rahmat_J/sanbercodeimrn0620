import React, { Component } from 'react';
import { View, Text, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native';

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                <ScrollView>
                    <View style={{ alignItems: 'center', justifyContent: 'center', marginHorizontal: 40 }}>
                        <Image source={require('./Images/logo.png')} style={{ marginVertical: 60, height: 116 }} />
                        <Text style={{ fontSize: 24, color: '#003366' }}>Login</Text>
                    </View>
                    <View style={{ marginVertical: 40 }}>
                        <View style={{ marginHorizontal: 40, marginBottom: 16 }}>
                            <Text style={{ fontSize: 16, color: '#003366' }}>username / Email</Text>
                            <TextInput
                                placeholder="Input Username / Email"
                                style={{
                                    height: 48,
                                    marginTop: 5,
                                    borderWidth: 1,
                                    borderColor: '#003366',
                                    padding: 10,
                                }}
                            />
                        </View>
                        <View style={{ marginHorizontal: 40, marginVertical: 16 }}>
                            <Text style={{ fontSize: 16, color: '#003366' }}>Password</Text>
                            <TextInput
                                placeholder="Input Passowrd"
                                style={{
                                    height: 48,
                                    marginTop: 5,
                                    borderWidth: 1,
                                    borderColor: '#003366',
                                    padding: 10,
                                }}
                            />
                        </View>
                    </View>
                    <View style={{ alignItems: 'center' }}>
                        <TouchableOpacity
                            style={{
                                borderRadius: 16,
                                height: 40,
                                width: 140,
                                backgroundColor: '#3EC6FF',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Text style={{ fontSize: 24, color: '#FFFFFF' }}>Masuk</Text>
                        </TouchableOpacity>
                        <Text style={{ fontSize: 24, color: '#3EC6FF', marginVertical: 16 }}>Atau</Text>
                        <TouchableOpacity
                            style={{
                                borderRadius: 16,
                                height: 40,
                                width: 140,
                                backgroundColor: '#003366',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <Text style={{ fontSize: 24, color: '#FFFFFF' }}>Daftar</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
