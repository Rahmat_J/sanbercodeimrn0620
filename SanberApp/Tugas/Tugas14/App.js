import React, { Component } from 'react';
import { View, Text } from 'react-native';
import Main from './components/Main';

export default class App extends Component {
  render() {
    return (
      <Main />
    );
  }
}
