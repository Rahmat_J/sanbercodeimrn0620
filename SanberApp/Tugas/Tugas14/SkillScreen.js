import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, FlatList } from 'react-native';
import { Ionicons, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';
import data from './skillData.json';

export default class SkillScreen extends Component {
    renderItem = ({ item }) => {
        return (
            <TouchableOpacity style={{ height: 129, backgroundColor: '#B4E9FF', borderRadius: 8, flexDirection: 'row', marginBottom: 8 }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <MaterialCommunityIcons name={item.iconName} size={74} color="#003366" />
                </View>
                <View style={{ flex: 2, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 24, fontWeight: 'bold', color: '#003366' }}>{item.skillName}</Text>
                    <Text style={{ fontSize: 16, fontWeight: 'bold', color: '#3EC6FF' }}>{item.categoryName}</Text>
                    <Text style={{ textAlign: 'right', fontSize: 48, color: '#FFFFFF' }}>{item.percentageProgress}</Text>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Ionicons name="ios-arrow-forward" size={74} color="#003366" />
                </View>
            </TouchableOpacity>
        )

    }

    render() {
        return (
            <ScrollView>
                <View style={{ flex: 1, backgroundColor: '#FFFFFF', paddingHorizontal: 16 }}>
                    <Image source={require('../Tugass13/Images/logo.png')} style={{ width: 187, height: 51, alignSelf: 'flex-end' }} />
                    <View style={{ flexDirection: 'row', marginTop: 7, marginBottom: 16, alignItems: 'center' }}>
                        <MaterialIcons name="account-circle" size={24} color="#3EC6FF" style={{ marginRight: 10 }} />
                        <View>
                            <Text style={{ fontSize: 12 }}>Hai,</Text>
                            <Text style={{ fontSize: 16, color: '#003366' }}>Rahmat Januardi</Text>
                        </View>
                    </View>
                    <Text style={{ fontSize: 36, color: '#003366' }}>Skill</Text>
                    <View style={{ borderWidth: 4, borderColor: '#3EC6FF', marginTop: 3 }} />
                    <View style={{ flexDirection: 'row', marginVertical: 10, justifyContent: 'space-between' }}>
                        <TouchableOpacity style={{ borderRadius: 8, height: 32, width: 125, alignItems: 'center', justifyContent: 'center', backgroundColor: '#B4E9FF' }}>
                            <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Library/FrameWork</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ borderRadius: 8, height: 32, width: 136, alignItems: 'center', justifyContent: 'center', marginHorizontal: 6, backgroundColor: '#B4E9FF' }}>
                            <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Bahsa Pemrograman</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ borderRadius: 8, height: 32, width: 70, alignItems: 'center', justifyContent: 'center', backgroundColor: '#B4E9FF' }}>
                            <Text style={{ fontSize: 12, fontWeight: 'bold' }}>Teknologi</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginBottom: 30 }}>
                        <FlatList
                            data={data.items}
                            renderItem={this.renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }
}
