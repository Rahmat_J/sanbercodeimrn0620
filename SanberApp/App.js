import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
// import TodoApp from './Tugas/Tugas14/App';
// import RegisterScreen from './Tugas/Tugass13/RegisterScreen';
// import LoginScreen from './Tugas/Tugass13/LoginScreen';
// import AboutScreen from './Tugas/Tugass13/AboutScreen';
// import SkillScreen from './Tugas/Tugas14/SkillScreen';
import Quiz from './Quiz_3'

export default function App() {
  return (
    <View style={{ flex: 1 }}>
      <StatusBar backgroundColor="#FFFFFF" translucent={false} />
      <Quiz />
    </View>
  );
}