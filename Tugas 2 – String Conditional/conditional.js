// if-else
var nama = "alex"
var peran = "Guard"

if (nama !== "" && peran == "") {
    console.log(`Hello ${nama}, Pilih peranmu untuk memulai game!`);
} else if (nama == '') {
    console.log('Nama Harus diisi!');
} else if (nama !== "" && peran == "penyihir") {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo ${peran} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
} else if (nama !== "" && peran == "Guard") {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo ${peran} ${nama},kamu akan membantu melindungi temanmu dari serangan werewolf.`);
} else if (nama !== "" && peran == "Werewolf") {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo ${peran} ${nama},Kamu akan memakan mangsa setiap malam!`);
} else {
    console.log('Peran tidak ada!');

}

// Switch Case
var tanggal = 16
var bulan = 1
var tahun = 1900

switch (true) {
    case tanggal > 31:
        tanggal = "-"
        break;
    default:
        tanggal = String(tanggal)
        break;
}

switch (true) {
    case tahun < 1900:
        tahun = "-"
        break;
    case tahun > 2200:
        tahun = "-"
        break;
    default:
        tahun = String(tahun)
        break;
}

switch (bulan) {
    case 1:
        bulan = "Januari"
        break;
    case 2:
        bulan = "Februari"
        break;
    case 3:
        bulan = "Maret"
        break;
    case 4:
        bulan = "April"
        break;
    case 5:
        bulan = "Mei"
        break;
    case 6:
        bulan = "Juni"
        break;
    case 7:
        bulan = "Juli"
        break;
    case 8:
        bulan = "Agustus"
        break;
    case 9:
        bulan = "September"
        break;
    case 10:
        bulan = "Oktober"
        break;
    case 11:
        bulan = "November"
        break;
    case 12:
        bulan = "Desember"
        break;
    default:
        console.log('bulan Tidak ada')
        break;
}
console.log(tanggal + ' ' + bulan + ' ' + tahun);
