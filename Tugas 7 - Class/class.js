// Release 0
console.log("Soal 1. Release 0");
console.log('---------------');
class Animal {
    constructor(_name) {
        let _legs = 4;
        let _cold_blooded = false

        this.name = _name;
        this.kaki = _legs;
        this.coldBlooded = _cold_blooded;
    }
    get legs() {
        return this.kaki
    }

    get cold_blooded() {
        return this.coldBlooded
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// Release 1
console.log("\nSoal 1. Release 1");
console.log('---------------');
class Ape extends Animal {
    constructor(_name, legs_kera, cold_blooded) {
        super(legs_kera, cold_blooded)
        var legs_kera = 2;
        this.name = _name
        this.kaki_kera = legs_kera;
    }

    get legs() {
        return this.kaki_kera;
    }

    yell() {
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(_name, _legs, _cold_blooded) {
        super(_legs, _cold_blooded)
        this.name = _name
    }
    jump() {
        console.log("hop hop");
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.name);
console.log(sungokong.legs);
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
console.log(kodok.name);
console.log(kodok.legs);
kodok.jump() // "hop hop"



// Soal 2
console.log("\nSoal 2");
console.log('---------------');
class Clock {
    constructor({template}) {
        this._template = template;
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this._timer);
    };

    start() {
        this.render.bind(this);
        this._timer = setInterval(this.render.bind(this), 1000);
    };

}

var clock = new Clock({ template: 'h:m:s' });
clock.start(); 