// Soal 1 Mengubah fungsi menjadi fungsi arrow
console.log("No. 1 - Mengubah fungsi menjadi fungsi arrow");
console.log("----------------------------------------");
const golden = goldenFunction = () => {
    console.log("this is golden!!")
}
golden()

// Soal 2 Sederhanakan menjadi Object literal di ES6
console.log("\nNo. 2 - Sederhanakan menjadi Object literal di ES6");
console.log("----------------------------------------");
const newFunction = (firstName, lastName) => {
    return {
        fullName() {
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

// Soal 3 Destructuring
console.log("\nNo. 3 - Destructuring");
console.log("----------------------------------------");
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const { firstName, lastName, destination, occupation } = newObject;
// Driver code
console.log(firstName, lastName, destination, occupation);

// Soal 4 Destructuring
console.log("\nNo. 4 -Array Spreading");
console.log("----------------------------------------");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// Soal 5 Template Literals
console.log("\nNo. 5 - Template Literals");
console.log("----------------------------------------");
const planet = "earth"
const view = "glass"
const before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`

// Driver Code
console.log(before) 