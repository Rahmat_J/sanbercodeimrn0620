var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var i = 0;
function panggilKembali(time) {
    if (i < books.length) {
        readBooks(time, books[i], function callback(timeSpent) {
            if (timeSpent > 0) {
                panggilKembali(10000)
            }
            else if ((timeSpent || books.length) == 0) {
                console.log("Tidak ada lagi buku yang harus dibaca");
            }
        })
    }
    i++
}
panggilKembali(10000)