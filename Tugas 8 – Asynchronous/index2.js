var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

var i = 0;
function panggilkembali(time) {
    if (i < books.length) {
        readBooksPromise(time, books[i])
            .then(hasil => panggilkembali(hasil))
            .catch(error => console.log(error))
    }
    i++
}
panggilkembali(10000);