// Soal no. 1
console.log('Soal No.1 Looping While');
console.log('--------------------------\n');
console.log('LOOPING PERTAMA');
var looping1 = 0;
var looping2 = 22;
while (looping1 < 20) {
    looping1 += 2;
    console.log(looping1 + ' - I love coding');
}
console.log('LOOPING KEDUA');
while (looping2 > 2) {
    looping2 -= 2;
    console.log(looping2 + ' - I will become a mobile developer');
}
console.log('\n===================================\n');

// Soal No.2
console.log('Soal No.2 Looping For');
console.log('--------------------------\n');
for (var i = 1; i <= 20; i++) {
    if ((i % 3) == 0 && (i % 2) == 1) {
        console.log(i + ' - I love coding');
    } else if ((i % 2) == 0) {
        console.log(i + ' - Berkualitas');
    } else if ((i % 2) !== 0) {
        console.log(i + ' - Santai');
    }
}
console.log('\n===================================\n');

// Soal No.3
console.log('Soal No.3');
console.log('--------------------------\n');
var pgr = ''
for (var j = 0; j < 4; j++) {
    for (var k = 0; k < 8; k++) {
        pgr += '#'
    }
    pgr += '\n'
}
console.log(pgr);
console.log('\n===================================\n');

// Soal No.4
console.log('Soal No.4');
console.log('--------------------------\n');
var pgr2 = ''
for (var l = 0; l < 7; l++) {
    for (var m = 0; m < l; m++) {
        pgr2 += '#'
    }
    pgr2 += '\n'
}
console.log(pgr2);
console.log('\n===================================\n');

// Soal No.5
console.log('Soal No.5');
console.log('--------------------------\n');
var catur = ''
for (var x = 0; x < 8; x++) {
    for (var y = 0; y < 8; y++) {
        if((y % 2 == 0 && x % 2 == 0) || (x % 2 == 1 && y % 2 == 1)){
            catur += ' '
        } else {
            catur += '#'
        }
    }
    catur += '\n'
}
console.log(catur);

