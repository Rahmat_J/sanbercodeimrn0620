//Soal 1
console.log('soal 1 - Range');
console.log('----------------');
function range(startNum, finishNum) {
    var arrayRange = [];
    if (startNum < finishNum) {
        for (var x = startNum; x <= finishNum; x++) {
            arrayRange.push(x)
        }
    } else if (startNum > finishNum) {
        for (var y = startNum; y >= finishNum; y--) {
            arrayRange.push(y)
        }
    } else {
        return -1
    }
    return arrayRange;
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// Soal 2
console.log('\nsoal 2 - Range with Step');
console.log('----------------');
function rangeWithStep(startNum, finishNum, step) {
    var arrayRange = [];
    if (startNum < finishNum) {
        for (var x = startNum; x <= finishNum; x += step) {
            arrayRange.push(x)
        }
    } else {
        for (var y = startNum; y >= finishNum; y -= step) {
            arrayRange.push(y)
        }
    }
    return arrayRange;
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

// Soal 3
console.log('\nsoal 3 - Sum of Range');
console.log('----------------');
function sum(startNum = 0, finishNum = 0, step = 1) {
    var hasilSum = 0;
    var arrayRange = rangeWithStep(startNum, finishNum, step);
    for (var z = 0; z < arrayRange.length; z++) {
        hasilSum += arrayRange[z];
    }
    return hasilSum
}
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

//soal 4
console.log('\nsoal 4 - Array Multidimensi');
console.log('----------------');
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling() {
    for (var x = 0; x < input.length; x++) {
        console.log("Nomor ID: " + input[x][0]);
        console.log("Nama Lengkap: " + input[x][1])
        console.log("TTL: " + input[x][2] + " " + input[x][4])
        console.log("Hobi: " + input[x][5])
        console.log('\n');
    }
}
dataHandling();

// soal 5
console.log('\nsoal 5 - Balik Kata');
console.log('----------------');
function balikKata(kata) {
    var hasil = " "
    for (var i = kata.length - 1; i >= 0; i--) {
        hasil += kata[i];
    }
    return hasil
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

// soal 6
console.log('\nsoal 6 - Metode Array');
console.log('----------------');
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(array) {
    var tanggal = []
    var bulan
    var parseBulan
    var stringNama
    
    // Splice
    array.splice(1, 1, `${array[1]} Elsharawy`)
    array.splice(2, 1, `Provinsi ${array[2]}`)
    array.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(array)

    // Split
    tanggal = array[3].split("/")
    bulan = parseInt(tanggal[1])
    switch (bulan) {
        case 1: {
            parseBulan = "Januari";
            break;
        }
        case 2: {
            parseBulan = "Februari";
            break;
        }
        case 3: {
            parseBulan = "Maret";
            break;
        }
        case 4: {
            parseBulan = "April";
            break;
        }
        case 5: {
            parseBulan = "Mei";
            break;
        }
        case 6: {
            parseBulan = "Juni";
            break;
        }
        case 7: {
            parseBulan = "Juli";
            break;
        }
        case 8: {
            parseBulan = "Agustus";
            break;
        }
        case 9: {
            parseBulan = "September";
            break;
        }
        case 10: {
            parseBulan = "Oktober";
            break;
        }
        case 11: {
            parseBulan = "November";
            break;
        }
        case 12: {
            parseBulan = "Desember";
            break;
        }
        default: {
            parseBulan = "";
        }
    }
    console.log(parseBulan)

    // Sorting
    tanggal.sort(
        function (a, b) {
            return b - a
        }
    )
    console.log(tanggal)

    // Join
    tanggal = (array[3].split("/")).join("-")
    console.log(tanggal)

    // Slice
    stringNama = String(array[1])
    console.log(stringNama.slice(0, 15))
}

// input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input)